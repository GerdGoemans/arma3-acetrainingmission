/*
    Author: stanhope
*/

//======================Zeus
#include "\arma3_readme.txt";

zeusModules = [zeus_1, zeus_2, zeus_3, zeus_4, zeus_5, zeus_6, zeus_7, zeus_8, zeus_9, zeus_10, zeus_11, zeus_12, zeus_13, zeus_14, zeus_15, zeus_16, zeus_17, zeus_18, zeus_19, zeus_20, zeus_21, zeus_22, zeus_23, zeus_24, zeus_25, zeus_26, zeus_27, zeus_28, zeus_29, zeus_30, zeus_31, zeus_32, zeus_33, zeus_34, zeus_35, zeus_36, zeus_37, zeus_38, zeus_39, zeus_40, zeus_41, zeus_42, zeus_43, zeus_44, zeus_45];

initiateZeusByUID = {
	params ["_player"];
	private _uid = getPlayerUID _player;

	if !( isNil {_player getVariable 'zeusModule'} ) then {
	    unassignCurator (zeusModules select (_player getVariable 'zeusModule'));
	};
	_player setVariable ["zeusModule", nil];
	_player setVariable ["isCoreStaff", false, true];
	_player setVariable ["isAdmin", false, true];
    _player setVariable ["isZeus", false, true];

	private _zeusUIDs = zeusCoreStaffUIDs + zeusAdminUIDs + zeusModeratorUIDs + zeusSpartanUIDs;
	private _zeusModuleNumber = _zeusUIDs find _uid;

	if ( _zeusModuleNumber == -1 ) exitWith {
	    [parseText format ["<t align='center' font='PuristaBold' ><t size='1.6'>%1</t><br />
            <t size='1.2'>Welcome %2</t><br /><t size='0.8' font='PuristaMedium'>Ensure you are familiar with our server rules:<br /> www.ahoyworld.net/index/rules</t>",
            "INVADE AND ANNEX", name _player],
            true, nil, 12, 0.3, 0.3
        ] remoteExec ["BIS_fnc_textTiles", _player];
    };

	private _zeusModule = zeusModules select _zeusModuleNumber;
	unassignCurator _zeusModule;
	_player assignCurator _zeusModule;
	_player setVariable ["zeusModule", _zeusModuleNumber];
	[_zeusModule,[-1,-2,0]] call BIS_fnc_setCuratorVisionModes;
	adminChannelID radioChannelAdd [_player];

    if ( (zeusCoreStaffUIDs find _uid) > -1 ) exitWith{
        _player setVariable ["isCoreStaff", true, true];
        _player setVariable ["isAdmin", true, true];
        _player setVariable ["isZeus", true, true];
        diag_log format ['Zeus (admin) assigned on %1', name _player];
        [parseText format ["<br /><t align='center' font='PuristaBold' ><t size='1.6'>%1</t><br />
            <t size='1.2'>Welcome %2</t>", "ZEUS (CORE STAFF) ASSIGNED", name _player], true, nil, 12, 0.3, 0.3
        ] remoteExec ["BIS_fnc_textTiles", _player];
    };

	if ( (zeusAdminUIDs find _uid) > -1 ) exitWith{
	    _player setVariable ["isAdmin", true, true];
		_player setVariable ["isZeus", true, true];
		diag_log format ['Zeus (admin) assigned on %1', name _player];
		[parseText format ["<br /><t align='center' font='PuristaBold' ><t size='1.6'>%1</t><br />
            <t size='1.2'>Welcome %2</t>", "ZEUS (PUBLIC MOD) ASSIGNED", name _player], true, nil, 12, 0.3, 0.3
        ] remoteExec ["BIS_fnc_textTiles", _player];
	};

	if ( (zeusModeratorUIDs find _uid) > -1 ) exitWith{
        [parseText format ["<br /><t align='center' font='PuristaBold' ><t size='1.6'>%1</t><br />
            <t size='1.2'>Welcome %2</t>", "ZEUS (AWE MODERATOR) ASSIGNED", name _player],
            true, nil, 12, 0.3, 0.3
        ] remoteExec ["BIS_fnc_textTiles", _player];
    };

	if ( (zeusSpartanUIDs find _uid) > -1 ) exitWith{
	    _player setVariable ["isZeus", true, true];
		diag_log format ['Zeus (spartan) assigned on %1', name _player];
		[parseText format ["<br /><t align='center' font='PuristaBold' ><t size='1.6'>%1</t><br />
            <t size='1.2'>Welcome %2</t>", "ZEUS (SPARTAN) ASSIGNED", name _player], true, nil, 12, 0.3, 0.3
        ] remoteExec ["BIS_fnc_textTiles", _player];
	};
};

[] spawn {
    waitUntil {!isNil "addToZeusFnc"};
    [addToZeus_1, addToZeus_2, addToZeus_3, addToZeus_4, addToZeus_5, addToZeus_6] call addToZeusFnc;

    ["ace_unconscious", {
        params ["_unit", "_state"];
        if (typeOf _unit == "C_Soldier_VR_F") then {
            if (count ((getPos _unit) nearObjects ["Land_InfoStand_V1_F", 20]) > 1) then {
                if (_state) then {
                    _unit switchMove "AinjPpneMstpSnonWnonDnon";
                } else {
                    _unit switchMove "";
                };
            };
        };
    }] call CBA_fnc_addEventHandler;

    {
        _x allowDamage false;
        private _unit = nearestObject [getPos _x, "C_Soldier_VR_F"];
        _x setVariable ["_unit", _unit];
        _x setVariable ["_unitPosWorld", getPosWorld _unit];
        waitUntil {sleep 0.1; !isNil "medicalFncRespawnUnit"};
        [_x] spawn medicalStopStation;

        [_x, ["Start station", { [_this, medicalStartStation] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "_target getVariable ['_disabled', true]", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Stop station", { [_this, medicalStopStation] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "!(_target getVariable ['_disabled', true])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Respawn unit", { [_this, medicalFncRespawnUnit] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "!(_target getVariable ['_disabled', true])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Add minor damage", { [_this, medicalAddMinorDamageToUnit] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "!(_target getVariable ['_disabled', true])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Add medium damage", { [_this, medicalAddMediumDamageToUnit] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "!(_target getVariable ['_disabled', true])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Add major damage", { [_this, medicalAddMajorDamageToUnit] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "!(_target getVariable ['_disabled', true])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Add random damage", { [_this, medicalAddRandomDamageToUnit] remoteExec ["spawn", 2]; }, nil, 0, true, true, "", "!(_target getVariable ['_disabled', true])", 2]] remoteExec ["addAction", 0, true];

        _x setObjectTextureGlobal [0, "media\stationSettings.jpg"];
    } forEach (entities "Land_InfoStand_V1_F");

    {
        [_x, ["Grab medical supplies", { _this spawn medicalSpawnBackPack; }, nil, 0, true, true, "", "true", 4]] remoteExec ["addAction", 0, true];
        [_x, ["Make me doctor", { _this spawn medicalChangeMedicalLevel; }, [2], 0, true, true, "", "true", 4]] remoteExec ["addAction", 0, true];
        [_x, ["Make me medic", { _this spawn medicalChangeMedicalLevel; }, [1], 0, true, true, "", "true", 4]] remoteExec ["addAction", 0, true];
        [_x, ["Make me rifleman", { _this spawn medicalChangeMedicalLevel; }, [0], 0, true, true, "", "true", 4]] remoteExec ["addAction", 0, true];
    } forEach [aceMedicalBox_1, aceMedicalBox_2];

    {
        _x setObjectTextureGlobal [0, "media\medicalArea.jpg"];
    } forEach [medicalArea1, medicalArea2];

    {
        [_x, ["Show bandages chart (simple)", { [_this, medicalShowSlide] remoteExec ["spawn", 2]; }, ["bandagechart_simple.jpg"], 0, true, true, "", "true", 2]] remoteExec ["addAction", 0, true];
        [_x, ["Show bandages chart (advanced)", { [_this, medicalShowSlide] remoteExec ["spawn", 2]; }, ["bandagechart_advanced.jpg"], 0, true, true, "", "true", 2]] remoteExec ["addAction", 0, true];
		[_x, ["Show bloodpressure chart", { [_this, medicalShowSlide] remoteExec ["spawn", 2]; }, ["bloodPressure.jpg"], 0, true, true, "", "true", 2]] remoteExec ["addAction", 0, true];
    } forEach [ teachingArea_1_Laptop, teachingArea_2_Laptop];


    {
        if (_x != spawnTPLaptop) then {
            [_x, ["TP to spawn", { (_this select 1) setPos (getPos spawnTPTarget); }, nil, -10, true, true, "", "missionNamespace getVariable ['_spawnTPTargetEnabled', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];
        };
        if (_x != grenadeRangeTPLaptop) then {
            [_x, ["TP to grenadeRange", { (_this select 1) setPos (getPos grenadeRangeTPTarget); }, nil, -10, true, true, "", "missionNamespace getVariable ['_grenadeRangeTPTargetEnabled', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];
        };
        if (_x != medicalAreaTPLaptop_1) then {
            [_x, ["TP to medical training area 1", { (_this select 1) setPos (getPos medicalAreaTPTarget_1); }, nil, -10, true, true, "", "missionNamespace getVariable ['_medicalAreaTPTarget_1Enabled', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];
        };
        if (_x != medicalAreaTPLaptop_2) then {
            [_x, ["TP to medical training area 2", { (_this select 1) setPos (getPos medicalAreaTPTarget_2); }, nil, -10, true, true, "", "missionNamespace getVariable ['_medicalAreaTPTarget_2Enabled', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];
        };
        if (_x != medicalTeachingAreaTPLaptop_1) then {
            [_x, ["TP to medical teaching area 1", { (_this select 1) setPos (getPos medicalTeachingAreaTPTarget_1); }, nil, -10, true, true, "", "missionNamespace getVariable ['_medicalTeachingAreaTPTarget_1Enabled', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];
        };
        if (_x != medicalTeachingAreaTPLaptop_2) then {
            [_x, ["TP to medical teaching area 2", { (_this select 1) setPos (getPos medicalTeachingAreaTPTarget_2); }, nil, -10, true, true, "", "missionNamespace getVariable ['_medicalTeachingAreaTPTarget_2Enabled', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];
        };

        [_x, ["<t color='#009ACD'>Enable this teleport laptop</t>", { (_this select 0) setVariable ["_thisLaptopEnabled", true, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(_target getVariable ['_thisLaptopEnabled', true])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable this teleport laptop</t>", { (_this select 0) setVariable ["_thisLaptopEnabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && _target getVariable ['_thisLaptopEnabled', true]", 2]] remoteExec ["addAction", 0, true];

        [_x, ["<t color='#009ACD'>Enable TP to spawn</t>", { missionNamespace setVariable ["_spawnTPTargetEnabled", true, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(missionNamespace getVariable ['_spawnTPTargetEnabled', false])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable TP to spawn</t>", { missionNamespace setVariable ["_spawnTPTargetEnabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && missionNamespace getVariable ['_spawnTPTargetEnabled', false]", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Enable TP to grenadeRange</t>", { missionNamespace setVariable ["_grenadeRangeTPTargetEnabled", true, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(missionNamespace getVariable ['_grenadeRangeTPTargetEnabled', false])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable TP to grenadeRange</t>", { missionNamespace setVariable ["_grenadeRangeTPTargetEnabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && missionNamespace getVariable ['_grenadeRangeTPTargetEnabled', false]", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Enable TP to medical training area 1</t>", { missionNamespace setVariable ["_medicalAreaTPTarget_1Enabled", true, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(missionNamespace getVariable ['_medicalAreaTPTarget_1Enabled', false])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable TP to medical training area 1</t>", { missionNamespace setVariable ["_medicalAreaTPTarget_1Enabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && missionNamespace getVariable ['_medicalAreaTPTarget_1Enabled', false]", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Enable TP to medical training area 2</t>", { missionNamespace setVariable ["_medicalAreaTPTarget_2Enabled", true, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(missionNamespace getVariable ['_medicalAreaTPTarget_2Enabled', false])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable TP to medical training area 2</t>", { missionNamespace setVariable ["_medicalAreaTPTarget_2Enabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && missionNamespace getVariable ['_medicalAreaTPTarget_2Enabled', false]", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Enable TP to medical teaching area 1</t>", { missionNamespace setVariable ["_medicalTeachingAreaTPTarget_1Enabled", true, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(missionNamespace getVariable ['_medicalTeachingAreaTPTarget_1Enabled', false])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable TP to medical teaching area 1</t>", { missionNamespace setVariable ["_medicalTeachingAreaTPTarget_1Enabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && missionNamespace getVariable ['_medicalTeachingAreaTPTarget_1Enabled', false]", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Enable TP to medical teaching area 2</t>", { missionNamespace setVariable ["_medicalTeachingAreaTPTarget_2Enabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(missionNamespace getVariable ['_medicalTeachingAreaTPTarget_2Enabled', false])", 2]] remoteExec ["addAction", 0, true];
        [_x, ["<t color='#009ACD'>Disable TP to medical teaching area 2</t>", { missionNamespace setVariable ["_medicalTeachingAreaTPTarget_2Enabled", false, true]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && missionNamespace getVariable ['_medicalTeachingAreaTPTarget_2Enabled', false]", 2]] remoteExec ["addAction", 0, true];
    } forEach [spawnTPLaptop, grenadeRangeTPLaptop, medicalAreaTPLaptop_1, medicalAreaTPLaptop_2, medicalTeachingAreaTPLaptop_1, medicalTeachingAreaTPLaptop_2];

    spawnTPLaptop setVariable ["_thisLaptopEnabled", false, true];
    missionNamespace setVariable ["_spawnTPTargetEnabled", true, true];

    private _targPosArray = [];
    {
        _targPosArray pushBack (getPosWorld _x);
        deleteVehicle _x;
    } forEach [grendadeRangeTarget_1, grendadeRangeTarget_2, grendadeRangeTarget_3, grendadeRangeTarget_4, grendadeRangeTarget_5, grendadeRangeTarget_6, grendadeRangeTarget_7, grendadeRangeTarget_8, grendadeRangeTarget_9, grendadeRangeTarget_10, grendadeRangeTarget_11, grendadeRangeTarget_12, grendadeRangeTarget_13, grendadeRangeTarget_14, grendadeRangeTarget_15, grendadeRangeTarget_16, grendadeRangeTarget_17, grendadeRangeTarget_18, grendadeRangeTarget_19, grendadeRangeTarget_20, grendadeRangeTarget_21, grendadeRangeTarget_22, grendadeRangeTarget_23, grendadeRangeTarget_24, grendadeRangeTarget_25, grendadeRangeTarget_26, grendadeRangeTarget_27];
    grenadeRangeSettingsLaptop setVariable ["_targPosArray", _targPosArray];

    [grenadeRangeSettingsLaptop, ["<t color='#009ACD'>Spawn targets", { [_this, spawnGrenadeTargets] remoteExec ["spawn", 2]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && !(_target getVariable ['_targetsAlreadySpawned', false])", 2]] remoteExec ["addAction", 0, true];
    [grenadeRangeSettingsLaptop, ["<t color='#009ACD'>Despawn targets", { [_this, despawnGrenadeTargets] remoteExec ["spawn", 2]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && _target getVariable ['_targetsAlreadySpawned', false]", 2]] remoteExec ["addAction", 0, true];
    [grenadeRangeSettingsLaptop, ["<t color='#009ACD'>Auto respawn targets", { [_this, autoRespawnGrenadeTargets] remoteExec ["spawn", 2]; }, nil, -20, true, true, "", "_this getVariable ['isZeus', false] && _target getVariable ['_targetsAlreadySpawned', false]", 2]] remoteExec ["addAction", 0, true];

};
