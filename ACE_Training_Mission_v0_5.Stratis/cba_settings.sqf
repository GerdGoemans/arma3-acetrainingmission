TFAR_default_radioVolume = 7;
TFAR_volumeModifier_force forceSpeech = false;
TFAR_intercomVolume = 0.3;
TFAR_pluginTimeout = 4;
TFAR_tangentReleaseDelay = 0;
TFAR_PosUpdateMode = 0.1;
TFAR_ShowVolumeHUD = false;
TFAR_VolumeHudTransparency = 0;
TFAR_oldVolumeHint = true;
TFAR_showTransmittingHint = true;
TFAR_showChannelChangedHint = true;
force force TF_terrain_interception_coefficient = 5;
force force TFAR_fullDuplex = false;
force force TFAR_enableIntercom = true;
force force TFAR_objectInterceptionEnabled = true;
force force TFAR_takingRadio = 2;
force force TFAR_spectatorCanHearEnemyUnits = true;
force force TFAR_spectatorCanHearFriendlies = true;
force force TFAR_giveMicroDagrToSoldier = true;
force force TFAR_givePersonalRadioToRegularSoldier = false;
force force TFAR_giveLongRangeRadioToGroupLeaders = false;
TFAR_setting_defaultFrequencies_sr_west = "50,60,70,80,90";
TFAR_setting_defaultFrequencies_sr_east = "50,60,70,80,90";
TFAR_setting_defaultFrequencies_sr_independent = "50,60,70,80,90";
force force TFAR_SameLRFrequenciesForSide = false;
TFAR_setting_defaultFrequencies_lr_west = "30,31";
TFAR_setting_defaultFrequencies_lr_east = "30,31";
TFAR_setting_defaultFrequencies_lr_independent = "30,31";
force force TFAR_setting_DefaultRadio_Rifleman_West = "TFAR_rf7800str";
force force TFAR_setting_DefaultRadio_Rifleman_East = "TFAR_pnr1000a";
force force TFAR_setting_DefaultRadio_Rifleman_Independent = "TFAR_anprc154";
force force TFAR_setting_DefaultRadio_Personal_West = "TFAR_rf7800str";
force force TFAR_setting_DefaultRadio_Personal_east = "TFAR_pnr1000a";
force force TFAR_setting_DefaultRadio_Personal_Independent = "TFAR_anprc154";
force force TFAR_setting_DefaultRadio_Backpack_west = "TFAR_rt1523g";
force force TFAR_setting_DefaultRadio_Backpack_east = "TFAR_mr3000";
force force TFAR_setting_DefaultRadio_Backpack_Independent = "TFAR_anprc155";
force force TFAR_setting_DefaultRadio_Airborne_West = "TFAR_anarc210";
force force TFAR_setting_DefaultRadio_Airborne_east = "TFAR_mr6000l";
force force TFAR_setting_DefaultRadio_Airborne_Independent = "TFAR_anarc164";
force force tfar_radioCodesDisabled = false;
force force tf_west_radio_code = "_bluefor";
force force tf_east_radio_code = "_opfor";
force force tf_independent_radio_code = "_guer";
force force TFAR_AICanHearPlayer = true;

STHud_Settings_Font = "PuristaSemibold";
STHud_Settings_HUDMode = 3;
force force STHud_Settings_Occlusion = false;
STHud_Settings_SquadBar = false;
force force STHud_Settings_RemoveDeadViaProximity = false;
STHud_Settings_TextShadow = 1;
STHud_Settings_ColourBlindMode = "Normal";

Achilles_var_moduleTreeHelmet = false;
Achilles_var_moduleTreeDLC = true;
Achilles_var_moduleTreeCollapse = true;
achilles_curator_vision_nvg = true;
achilles_curator_vision_whitehot = true;
achilles_curator_vision_blackhot = false;
achilles_curator_vision_greenhotcold = false;
achilles_curator_vision_blackhotgreencold = false;
achilles_curator_vision_redhot = false;
achilles_curator_vision_blackhotredcold = false;
achilles_curator_vision_whitehotredcold = false;
achilles_curator_vision_redgreen = false;

// ACE Advanced Ballistics
ace_advanced_ballistics_ammoTemperatureEnabled = true;
ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
ace_advanced_ballistics_bulletTraceEnabled = true;
ace_advanced_ballistics_enabled = false;
ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Fatigue

ace_advanced_fatigue_enabled = true;

ace_advanced_fatigue_enableStaminaBar = true;

ace_advanced_fatigue_fadeStaminaBar = true;

ace_advanced_fatigue_loadFactor = 0.5;

ace_advanced_fatigue_performanceFactor = 4.5;

ace_advanced_fatigue_recoveryFactor = 3;

ace_advanced_fatigue_swayFactor = 0.3;

ace_advanced_fatigue_terrainGradientFactor = 0.5;


// ACE Advanced Throwing
ace_advanced_throwing_enabled = true;
ace_advanced_throwing_enablePickUp = true;
ace_advanced_throwing_enablePickUpAttached = true;
ace_advanced_throwing_showMouseControls = true;
ace_advanced_throwing_showThrowArc = true;

// ACE Arsenal
ace_arsenal_allowDefaultLoadouts = true;
ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
ace_arsenal_enableIdentityTabs = true;
ace_arsenal_enableModIcons = true;
ace_arsenal_EnableRPTLog = false;
ace_arsenal_fontHeight = 4.5;

// ACE Artillery
ace_artillerytables_advancedCorrections = true;
ace_artillerytables_disableArtilleryComputer = true;
ace_mk6mortar_airResistanceEnabled = false;
ace_mk6mortar_allowCompass = true;
ace_mk6mortar_allowComputerRangefinder = true;
ace_mk6mortar_useAmmoHandling = false;

// ACE Captives
ace_captives_allowHandcuffOwnSide = true;
ace_captives_allowSurrender = true;
ace_captives_requireSurrender = 1;
ace_captives_requireSurrenderAi = false;

// ACE Common
ace_common_allowFadeMusic = true;
ace_common_checkPBOsAction = 0;
ace_common_checkPBOsCheckAll = false;
ace_common_checkPBOsWhitelist = "[]";
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [1,1,1,1];
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 0;
ace_noradio_enabled = true;
ace_parachute_hideAltimeter = true;

// ACE Cook off
ace_cookoff_ammoCookoffDuration = 1;
ace_cookoff_enable = 0;
ace_cookoff_enableAmmobox = true;
ace_cookoff_enableAmmoCookoff = true;
ace_cookoff_probabilityCoef = 1;

// ACE Crew Served Weapons
ace_csw_ammoHandling = 1;
ace_csw_defaultAssemblyMode = false;
ace_csw_dragAfterDeploy = false;
ace_csw_handleExtraMagazines = true;
ace_csw_progressBarTimeCoefficent = 1;

// ACE Explosives
ace_explosives_explodeOnDefuse = true;
ace_explosives_punishNonSpecialists = true;
ace_explosives_requireSpecialist = false;

// ACE Fragmentation Simulation
ace_frag_enabled = true;
ace_frag_maxTrack = 10;
ace_frag_maxTrackPerFrame = 10;
ace_frag_reflectionsEnabled = false;
ace_frag_spallEnabled = false;

// ACE Goggles
ace_goggles_effects = 2;
ace_goggles_showClearGlasses = false;
ace_goggles_showInThirdPerson = false;

// ACE Hearing
ace_hearing_autoAddEarplugsToUnits = true;
ace_hearing_disableEarRinging = false;
ace_hearing_earplugsVolume = 0.5;
ace_hearing_enableCombatDeafness = true;
ace_hearing_enabledForZeusUnits = false;
ace_hearing_unconsciousnessVolume = 0.4;

// ACE Interaction
ace_interaction_disableNegativeRating = true;
ace_interaction_enableMagazinePassing = true;
ace_interaction_enableTeamManagement = true;

// ACE Interaction Menu
ace_gestures_showOnInteractionMenu = 2;
ace_interact_menu_actionOnKeyRelease = true;
ace_interact_menu_addBuildingActions = false;
ace_interact_menu_alwaysUseCursorInteraction = false;
ace_interact_menu_alwaysUseCursorSelfInteraction = true;
ace_interact_menu_colorShadowMax = [0,0,0,1];
ace_interact_menu_colorShadowMin = [0,0,0,0.25];
ace_interact_menu_colorTextMax = [1,1,1,1];
ace_interact_menu_colorTextMin = [1,1,1,0.25];
ace_interact_menu_cursorKeepCentered = false;
ace_interact_menu_cursorKeepCenteredSelfInteraction = false;
ace_interact_menu_menuAnimationSpeed = 0;
ace_interact_menu_menuBackground = 0;
ace_interact_menu_menuBackgroundSelf = 0;
ace_interact_menu_selectorColor = [1,0,0];
ace_interact_menu_shadowSetting = 2;
ace_interact_menu_textSize = 2;
ace_interact_menu_useListMenu = true;
ace_interact_menu_useListMenuSelf = true;

// ACE Logistics
ace_cargo_enable = true;
ace_cargo_loadTimeCoefficient = 5;
ace_cargo_paradropTimeCoefficent = 2.5;
ace_rearm_distance = 20;
ace_rearm_level = 0;
ace_rearm_supply = 0;
ace_refuel_hoseLength = 12;
ace_refuel_rate = 1;
ace_repair_addSpareParts = true;
ace_repair_autoShutOffEngineWhenStartingRepair = false;
ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
ace_repair_engineerSetting_fullRepair = 2;
ace_repair_engineerSetting_repair = 1;
ace_repair_engineerSetting_wheel = 0;
ace_repair_fullRepairLocation = 2;
ace_repair_fullRepairRequiredItems = ["ToolKit"];
ace_repair_miscRepairRequiredItems = ["ToolKit"];
ace_repair_repairDamageThreshold = 0.6;
ace_repair_repairDamageThreshold_engineer = 0.4;
ace_repair_wheelRepairRequiredItems = [];

// ACE Magazine Repack
ace_magazinerepack_timePerAmmo = 1.5;
ace_magazinerepack_timePerBeltLink = 8;
ace_magazinerepack_timePerMagazine = 2;

// ACE Map
ace_map_BFT_Enabled = false;
ace_map_BFT_HideAiGroups = false;
ace_map_BFT_Interval = 1;
ace_map_BFT_ShowPlayerNames = false;
ace_map_DefaultChannel = -1;
ace_map_mapGlow = true;
ace_map_mapIllumination = true;
ace_map_mapLimitZoom = false;
ace_map_mapShake = true;
ace_map_mapShowCursorCoordinates = false;
ace_markers_moveRestriction = 0;

// ACE Map Gestures
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
ace_map_gestures_enabled = true;
ace_map_gestures_interval = 0.03;
ace_map_gestures_maxRange = 7;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
ace_medical_ai_enabledFor = 2;
ace_medical_AIDamageThreshold = 1;
ace_medical_bleedingCoefficient = 1;
ace_medical_blood_bloodLifetime = 480;
ace_medical_blood_enabledFor = 2;
ace_medical_blood_maxBloodObjects = 300;
ace_medical_fatalDamageSource = 0;
ace_medical_feedback_painEffectType = 0;
ace_medical_fractures = 1;
ace_medical_gui_enableActions = 0;
ace_medical_gui_enableMedicalMenu = 1;
ace_medical_gui_enableSelfActions = true;
ace_medical_gui_maxDistance = 3.47228;
ace_medical_gui_openAfterTreatment = true;
ace_medical_ivFlowRate = 1;
ace_medical_limping = 1;
ace_medical_painCoefficient = 1;
ace_medical_playerDamageThreshold = 1;
ace_medical_spontaneousWakeUpChance = 0.45;
ace_medical_spontaneousWakeUpEpinephrineBoost = 10.0;
ace_medical_statemachine_AIUnconsciousness = true;
ace_medical_statemachine_cardiacArrestTime = 300;
ace_medical_statemachine_fatalInjuriesAI = 0;
ace_medical_statemachine_fatalInjuriesPlayer = 0;
ace_medical_treatment_advancedBandages = true;
ace_medical_treatment_advancedDiagnose = true;
ace_medical_treatment_advancedMedication = true;
ace_medical_treatment_allowLitterCreation = true;
ace_medical_treatment_allowSelfIV = 1;
ace_medical_treatment_allowSelfStitch = 1;
ace_medical_treatment_allowSharedEquipment = 1;
ace_medical_treatment_clearTraumaAfterBandage = true;
ace_medical_treatment_consumePAK = 1;
ace_medical_treatment_consumeSurgicalKit = 1;
ace_medical_treatment_convertItems = 0;
ace_medical_treatment_cprSuccessChance = 0.4;
ace_medical_treatment_holsterRequired = 0;
ace_medical_treatment_litterCleanupDelay = 600;
ace_medical_treatment_locationEpinephrine = 0;
ace_medical_treatment_locationPAK = 2;
ace_medical_treatment_locationsBoostTraining = true;
ace_medical_treatment_locationSurgicalKit = 3;
ace_medical_treatment_maxLitterObjects = 500;
ace_medical_treatment_medicEpinephrine = 0;
ace_medical_treatment_medicPAK = 2;
ace_medical_treatment_medicSurgicalKit = 2;
ace_medical_treatment_timeCoefficientPAK = 1;
ace_medical_treatment_woundReopening = true;

// ACE Name Tags
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
ace_nametags_nametagColorMain = [1,1,1,1];
ace_nametags_nametagColorRed = [1,0.67,0.67,1];
ace_nametags_nametagColorYellow = [1,1,0.67,1];
ace_nametags_playerNamesMaxAlpha = 0.8;
ace_nametags_playerNamesViewDistance = 5;
ace_nametags_showCursorTagForVehicles = false;
ace_nametags_showNamesForAI = false;
ace_nametags_showPlayerNames = 1;
ace_nametags_showPlayerRanks = true;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 2;

// ACE Nightvision
ace_nightvision_aimDownSightsBlur = 0.220391;
ace_nightvision_disableNVGsWithSights = false;
ace_nightvision_effectScaling = 0.970993;
ace_nightvision_fogScaling = 0.180885;
ace_nightvision_noiseScaling = 0.167717;
ace_nightvision_shutterEffects = false;

// ACE Overheating
ace_overheating_displayTextOnJam = true;
ace_overheating_enabled = true;
ace_overheating_overheatingDispersion = true;
ace_overheating_showParticleEffects = true;
ace_overheating_showParticleEffectsForEveryone = false;
ace_overheating_unJamFailChance = 0.1;
ace_overheating_unJamOnreload = false;

// ACE Pointing
ace_finger_enabled = false;
ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
ace_finger_indicatorForSelf = true;
ace_finger_maxRange = 4;

// ACE Pylons
ace_pylons_enabledForZeus = true;
ace_pylons_enabledFromAmmoTrucks = true;
ace_pylons_rearmNewPylons = false;
ace_pylons_requireEngineer = false;
ace_pylons_requireToolkit = true;
ace_pylons_searchDistance = 15;
ace_pylons_timePerPylon = 5;

// ACE Quick Mount
ace_quickmount_distance = 3;
ace_quickmount_enabled = true;
ace_quickmount_enableMenu = 3;
ace_quickmount_priority = 0;
ace_quickmount_speed = 18;

// ACE Respawn
ace_respawn_removeDeadBodiesDisconnected = true;
ace_respawn_savePreDeathGear = false;

// ACE Scopes
ace_scopes_correctZeroing = true;
ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
ace_scopes_defaultZeroRange = 100;
ace_scopes_enabled = true;
ace_scopes_forceUseOfAdjustmentTurrets = false;
ace_scopes_overwriteZeroRange = false;
ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
ace_scopes_zeroReferenceBarometricPressure = 1013.25;
ace_scopes_zeroReferenceHumidity = 0;
ace_scopes_zeroReferenceTemperature = 15;

// ACE Spectator
ace_spectator_enableAI = false;
ace_spectator_maxFollowDistance = 5;
ace_spectator_restrictModes = 0;
ace_spectator_restrictVisions = 0;

// ACE Switch Units
ace_switchunits_enableSafeZone = true;
ace_switchunits_enableSwitchUnits = false;
ace_switchunits_safeZoneRadius = 100;
ace_switchunits_switchToCivilian = false;
ace_switchunits_switchToEast = false;
ace_switchunits_switchToIndependent = false;
ace_switchunits_switchToWest = false;

// ACE Uncategorized
ace_fastroping_requireRopeItems = false;
ace_gforces_enabledFor = 1;
ace_hitreactions_minDamageToTrigger = 0.1;
ace_inventory_inventoryDisplaySize = 0;
ace_laser_dispersionCount = 2;
ace_microdagr_mapDataAvailable = 2;
ace_microdagr_waypointPrecision = 3;
ace_optionsmenu_showNewsOnMainMenu = true;
ace_overpressure_distanceCoefficient = 1;
ace_tagging_quickTag = 1;

// ACE User Interface
ace_ui_allowSelectiveUI = true;
ace_ui_ammoCount = false;
ace_ui_ammoType = true;
ace_ui_commandMenu = true;
ace_ui_firingMode = true;
ace_ui_groupBar = false;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
ace_vehiclelock_defaultLockpickStrength = 10;
ace_vehiclelock_lockVehicleInventory = false;
ace_vehiclelock_vehicleStartingLockState = -1;

// ACE Vehicles
ace_vehicles_hideEjectAction = true;
ace_vehicles_keepEngineRunning = false;

// ACE View Distance Limiter
ace_viewdistance_enabled = true;
ace_viewdistance_limitViewDistance = 10000;
ace_viewdistance_objectViewDistanceCoeff = 0;
ace_viewdistance_viewDistanceAirVehicle = 0;
ace_viewdistance_viewDistanceLandVehicle = 0;
ace_viewdistance_viewDistanceOnFoot = 0;

// ACE Weapons
ace_common_persistentLaserEnabled = false;
ace_laserpointer_enabled = true;
ace_reload_displayText = true;
ace_reload_showCheckAmmoSelf = false;
ace_weaponselect_displayText = true;

// ACE Weather
ace_weather_enabled = true;
ace_weather_showCheckAirTemperature = true;
ace_weather_updateInterval = 60;
ace_weather_windSimulation = true;

// ACE Wind Deflection
ace_winddeflection_enabled = true;
ace_winddeflection_simulationInterval = 0.05;
ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
ace_zeus_autoAddObjects = false;
ace_zeus_canCreateZeus = -1;
ace_zeus_radioOrdnance = false;
ace_zeus_remoteWind = false;
ace_zeus_revealMines = 0;
ace_zeus_zeusAscension = false;
ace_zeus_zeusBird = false;

// ACEX Field Rations
acex_field_rations_affectAdvancedFatigue = true;
acex_field_rations_enabled = false;
acex_field_rations_hudShowLevel = 0;
acex_field_rations_hudTransparency = -1;
acex_field_rations_hudType = 0;
acex_field_rations_hungerSatiated = 1;
acex_field_rations_thirstQuenched = 1;
acex_field_rations_timeWithoutFood = 2;
acex_field_rations_timeWithoutWater = 2;

// ACEX Fortify
acex_fortify_settingHint = 2;

// ACEX Headless
acex_headless_delay = 15;
acex_headless_enabled = false;
acex_headless_endMission = 0;
acex_headless_log = false;
acex_headless_transferLoadout = 0;

// ACEX Sitting
acex_sitting_enable = false;

// ACEX View Restriction
acex_viewrestriction_mode = 0;
acex_viewrestriction_modeSelectiveAir = 0;
acex_viewrestriction_modeSelectiveFoot = 0;
acex_viewrestriction_modeSelectiveLand = 0;
acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = false;

// ACEX Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// Achilles - Available Factions
Achilles_var_BLU_CTRG_F = true;
Achilles_var_BLU_F = true;
Achilles_var_BLU_G_F = true;
Achilles_var_BLU_GEN_F = true;
Achilles_var_BLU_T_F = true;
Achilles_var_BLU_W_F = true;
Achilles_var_CIV_F = true;
Achilles_var_CIV_IDAP_F = true;
Achilles_var_IND_C_F = true;
Achilles_var_IND_E_F = true;
Achilles_var_IND_F = true;
Achilles_var_IND_G_F = true;
Achilles_var_IND_L_F = true;
Achilles_var_Interactive_F = true;
Achilles_var_OPF_F = true;
Achilles_var_OPF_G_F = true;
Achilles_var_OPF_R_F = true;
Achilles_var_OPF_T_F = true;
Achilles_var_OPF_V_F = true;
Achilles_var_rhs_faction_insurgents = true;
Achilles_var_rhs_faction_msv = true;
Achilles_var_rhs_faction_rva = true;
Achilles_var_rhs_faction_socom = true;
Achilles_var_rhs_faction_tv = true;
Achilles_var_rhs_faction_usaf = true;
Achilles_var_rhs_faction_usarmy = true;
Achilles_var_rhs_faction_usarmy_d = true;
Achilles_var_rhs_faction_usarmy_wd = true;
Achilles_var_rhs_faction_usmc = true;
Achilles_var_rhs_faction_usmc_d = true;
Achilles_var_rhs_faction_usmc_wd = true;
Achilles_var_rhs_faction_usn = true;
Achilles_var_rhs_faction_vdv = true;
Achilles_var_rhs_faction_vdv_45 = true;
Achilles_var_rhs_faction_vmf = true;
Achilles_var_rhs_faction_vpvo = true;
Achilles_var_rhs_faction_vv = true;
Achilles_var_rhs_faction_vvs = true;
Achilles_var_rhs_faction_vvs_c = true;
Achilles_var_rhsgref_faction_cdf_air = true;
Achilles_var_rhsgref_faction_cdf_air_b = true;
Achilles_var_rhsgref_faction_cdf_ground = true;
Achilles_var_rhsgref_faction_cdf_ground_b = true;
Achilles_var_rhsgref_faction_cdf_ng = true;
Achilles_var_rhsgref_faction_cdf_ng_b = true;
Achilles_var_rhsgref_faction_chdkz = true;
Achilles_var_rhsgref_faction_chdkz_g = true;
Achilles_var_rhsgref_faction_hidf = true;
Achilles_var_rhsgref_faction_nationalist = true;
Achilles_var_rhsgref_faction_tla = true;
Achilles_var_rhsgref_faction_un = true;
Achilles_var_rhssaf_faction_airforce = true;
Achilles_var_rhssaf_faction_airforce_opfor = true;
Achilles_var_rhssaf_faction_army = true;
Achilles_var_rhssaf_faction_army_opfor = true;
Achilles_var_rhssaf_faction_un = true;
Achilles_var_UK3CB_BAF_Faction = true;
Achilles_var_UK3CB_BAF_Faction_Airforce = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_Arctic = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_CS95_DPM = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_DDPM = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_Desert = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_DPM = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_MTP = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_Temperate = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_Tropical = true;
Achilles_var_UK3CB_BAF_Faction_Airforce_Woodland = true;
Achilles_var_UK3CB_BAF_Faction_Army_Arctic = true;
Achilles_var_UK3CB_BAF_Faction_Army_CS95_DPM = true;
Achilles_var_UK3CB_BAF_Faction_Army_DDPM = true;
Achilles_var_UK3CB_BAF_Faction_Army_Desert = true;
Achilles_var_UK3CB_BAF_Faction_Army_DPM = true;
Achilles_var_UK3CB_BAF_Faction_Army_MTP = true;
Achilles_var_UK3CB_BAF_Faction_Army_Temperate = true;
Achilles_var_UK3CB_BAF_Faction_Army_Tropical = true;
Achilles_var_UK3CB_BAF_Faction_Army_Woodland = true;
Achilles_var_UK3CB_BAF_Faction_Navy_Arctic = true;
Achilles_var_UK3CB_BAF_Faction_Navy_CS95_DPM = true;
Achilles_var_UK3CB_BAF_Faction_Navy_DDPM = true;
Achilles_var_UK3CB_BAF_Faction_Navy_Desert = true;
Achilles_var_UK3CB_BAF_Faction_Navy_DPM = true;
Achilles_var_UK3CB_BAF_Faction_Navy_MTP = true;
Achilles_var_UK3CB_BAF_Faction_Navy_Temperate = true;
Achilles_var_UK3CB_BAF_Faction_Navy_Tropical = true;
Achilles_var_UK3CB_BAF_Faction_Navy_Woodland = true;
Achilles_var_Virtual_F = true;

// Achilles - Available Modules
Achilles_var_Achilles_ACE_Heal_Module = true;
Achilles_var_Achilles_ACE_Injury_Module = true;
Achilles_var_Achilles_AddECM_Module = true;
Achilles_var_Achilles_Animation_Module = true;
Achilles_var_Achilles_Attach_To_Module = true;
Achilles_var_Achilles_Bind_Variable_Module = true;
Achilles_var_Achilles_Buildings_Destroy_Module = true;
Achilles_var_Achilles_Buildings_LockDoors_Module = true;
Achilles_var_Achilles_Buildings_ToggleLight_Module = true;
Achilles_var_Achilles_CAS_Module = true;
Achilles_var_Achilles_Change_Ability_Module = true;
Achilles_var_Achilles_Change_Altitude_Module = true;
Achilles_var_Achilles_Chatter_Module = true;
Achilles_var_Achilles_Create_Universal_Target_Module = true;
Achilles_var_Achilles_DevTools_FunctionViewer = true;
Achilles_var_Achilles_DevTools_ShowInAnimViewer = true;
Achilles_var_Achilles_DevTools_ShowInConfig = true;
Achilles_var_Achilles_Earthquake_Module = true;
Achilles_var_Achilles_Hide_Objects_Module = true;
Achilles_var_Achilles_IED_Module = true;
Achilles_var_Achilles_Make_Invincible_Module = true;
Achilles_var_Achilles_Module_Arsenal_AddFull = true;
Achilles_var_Achilles_Module_Arsenal_CopyToClipboard = true;
Achilles_var_Achilles_Module_Arsenal_CreateCustom = true;
Achilles_var_Achilles_Module_Arsenal_Paste = true;
Achilles_var_Achilles_Module_Arsenal_Remove = true;
Achilles_var_Achilles_Module_Change_Side_Relations = true;
Achilles_var_Achilles_Module_Equipment_Attach_Dettach_Effect = true;
Achilles_var_Achilles_Module_FireSupport_CASBomb = true;
Achilles_var_Achilles_Module_FireSupport_CASGun = true;
Achilles_var_Achilles_Module_FireSupport_CASGunMissile = true;
Achilles_var_Achilles_Module_FireSupport_CASMissile = true;
Achilles_var_Achilles_Module_Manage_Advanced_Compositions = true;
Achilles_var_Achilles_Module_Player_Set_Frequencies = true;
Achilles_var_Achilles_Module_Rotation = true;
Achilles_var_Achilles_Module_Spawn_Advanced_Composition = true;
Achilles_var_Achilles_Module_Spawn_Carrier = true;
Achilles_var_Achilles_Module_Spawn_Destroyer = true;
Achilles_var_Achilles_Module_Spawn_Effects = true;
Achilles_var_Achilles_Module_Spawn_Explosives = true;
Achilles_var_Achilles_Module_Spawn_Intel = true;
Achilles_var_Achilles_Module_Supply_Drop = true;
Achilles_var_Achilles_Module_Zeus_AssignZeus = true;
Achilles_var_Achilles_Module_Zeus_SwitchUnit = true;
Achilles_var_Achilles_Nuke_Module = true;
Achilles_var_Achilles_Set_Date_Module = true;
Achilles_var_Achilles_Set_Height_Module = true;
Achilles_var_Achilles_Set_Weather_Module = true;
Achilles_var_Achilles_Sit_On_Chair_Module = true;
Achilles_var_Achilles_SuicideBomber_Module = true;
Achilles_var_Achilles_Suppressive_Fire_Module = true;
Achilles_var_Achilles_Toggle_Simulation_Module = true;
Achilles_var_Achilles_Transfer_Ownership_Module = true;
Achilles_var_Ares_Artillery_Fire_Mission_Module = true;
Achilles_var_Ares_Module_Bahaviour_Garrison_Nearest = true;
Achilles_var_Ares_Module_Bahaviour_SurrenderUnit = true;
Achilles_var_Ares_Module_Bahaviour_UnGarrison = true;
Achilles_var_Ares_Module_Behaviour_Patrol = true;
Achilles_var_Ares_Module_Behaviour_Search_Nearby_And_Garrison = true;
Achilles_var_Ares_Module_Behaviour_Search_Nearby_Building = true;
Achilles_var_Ares_Module_Dev_Tools_Create_Mission_SQF = true;
Achilles_var_Ares_Module_Dev_Tools_Execute_Code = true;
Achilles_var_Ares_Module_Equipment_Flashlight_IR_ON_OFF = true;
Achilles_var_Ares_Module_Equipment_NVD_TACLIGHT_IR = true;
Achilles_var_Ares_Module_Equipment_Turret_Optics = true;
Achilles_var_Ares_Module_Player_Change_Player_Side = true;
Achilles_var_Ares_Module_Player_Create_Teleporter = true;
Achilles_var_Ares_Module_Player_Teleport = true;
Achilles_var_Ares_Module_Reinforcements_Create_Lz = true;
Achilles_var_Ares_Module_Reinforcements_Create_Rp = true;
Achilles_var_Ares_Module_Reinforcements_Spawn_Units = true;
Achilles_var_Ares_Module_Spawn_Submarine = true;
Achilles_var_Ares_Module_Spawn_Trawler = true;
Achilles_var_Ares_Module_Zeus_Add_Remove_Editable_Objects = true;
Achilles_var_Ares_Module_Zeus_Hint = true;
Achilles_var_Ares_Module_Zeus_Switch_Side = true;
Achilles_var_Ares_Module_Zeus_Visibility = true;
Achilles_var_ModulePunishment_F = true;

// Achilles - Curator Vision Modes
achilles_curator_vision_blackhot = false;
achilles_curator_vision_blackhotgreencold = false;
achilles_curator_vision_blackhotredcold = false;
achilles_curator_vision_greenhotcold = false;
achilles_curator_vision_nvg = true;
achilles_curator_vision_redgreen = false;
achilles_curator_vision_redhot = false;
achilles_curator_vision_whitehot = true;
achilles_curator_vision_whitehotredcold = false;

// Achilles - Debug
Achilles_Debug_Output_Enabled = false;

// Achilles - Module Defaults
Achilles_var_setRadioFrequenciesLR_Default = "50";
Achilles_var_setRadioFrequenciesSR_Default = "150";

// Achilles - User Interface
Achilles_var_iconSelection = "Achilles_var_iconSelection_Ares";
Achilles_var_moduleTreeCollapse = true;
Achilles_var_moduleTreeDLC = true;
Achilles_var_moduleTreeHelmet = false;
Achilles_var_moduleTreeSearchPatch = false;

// CBA UI
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// CBA Weapons
cba_disposable_dropUsedLauncher = 2;
cba_disposable_replaceDisposableLauncher = true;
cba_events_repetitionMode = 1;
cba_optics_usePipOptics = true;

// STUI Settings
STGI_Settings_Enabled = true;
STGI_Settings_UnconsciousFadeEnabled = true;
STHud_Settings_ColourBlindMode = "Normal";
STHud_Settings_Font = "PuristaSemibold";
STHud_Settings_HUDMode = 3;
STHud_Settings_Occlusion = true;
STHud_Settings_RemoveDeadViaProximity = true;
STHud_Settings_SquadBar = false;
STHud_Settings_TextShadow = 1;
STHud_Settings_UnconsciousFadeEnabled = true;

// TFAR - Clientside settings
TFAR_default_radioVolume = 6;
TFAR_intercomDucking = 0.2;
TFAR_intercomVolume = 0.1;
TFAR_moveWhileTabbedOut = false;
TFAR_oldVolumeHint = false;
TFAR_pluginTimeout = 4;
TFAR_PosUpdateMode = 0.1;
TFAR_showChannelChangedHint = true;
TFAR_ShowDiaryRecord = true;
TFAR_showTransmittingHint = true;
TFAR_ShowVolumeHUD = false;
TFAR_tangentReleaseDelay = 0;
TFAR_VolumeHudTransparency = 0;
TFAR_volumeModifier_forceSpeech = false;
