/*
	author: stanhope
*/
missionNamespace setVariable ['Ares_Allow_Zeus_To_Execute_Code', false];

if (!isNil {player getVariable "PlayerLoadout"}) then {
	player setUnitLoadout [player getVariable "PlayerLoadout", true];
};

[] spawn {
    waitUntil {sleep 1; time > 1};
    [[player], addToZeusFnc] remoteExec ["spawn", 2];

    [player] remoteExecCall ["initiateZeusByUID", 2];
    sleep 3;
    if (player getVariable ["isAdmin", false]) then {execVM "scripts\adminScripts.sqf";};

    if ( isNil "tfarEH" ) then {
        tfarEH = ["tfarUpdate", "OnRadiosReceived",{
            [] spawn {
                sleep 1;
                if ( call TFAR_fnc_haveSWRadio ) then {	call RYK_fnc_TFAR_SR; };
                if ( call TFAR_fnc_haveLRRadio ) then {	call RYK_fnc_TFAR_LR; };
            };
        }, player] call TFAR_fnc_addEventHandler;
    };
    // setup radios in case TFAR eventhandlers never work again :(
    if ( call TFAR_fnc_haveSWRadio ) then {	call RYK_fnc_TFAR_SR; };
    if ( call TFAR_fnc_haveLRRadio ) then {	call RYK_fnc_TFAR_LR; };

    //chat
    0 enableChannel [false, false];
    1 enableChannel [true, false];
    2 enableChannel [true, false];
    3 enableChannel [true, false];
    4 enableChannel [true, false];
    5 enableChannel [true, false];
};


