/*
    Author: stanhope
*/

addToZeusFnc = {
    {_x addCuratorEditableObjects [_this, true];} forEach allCurators;
};

medicalShowSlide = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _billboards = [];
    if (_target == teachingArea_1_Laptop) then {_billboards = [teachingArea_1_display_1, teachingArea_1_display_2];};
    if (_target == teachingArea_2_Laptop) then {_billboards = [teachingArea_2_display_1, teachingArea_2_display_2];};
    if (count _billboards == 0) exitWith {diag_log format ["Error: medicalShowSlide: Did not recognize %1 as a _target", str _target];};

    {
        _x setObjectTextureGlobal [0, format ["media\slides\%1", _arguments select 0]];
    } forEach _billboards;
};

medicalStopStation = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _unit = _target getVariable ["_unit", objNull];
    deleteVehicle _unit;
    _target setVariable ["_disabled", true, true];
};

medicalStartStation = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _pos = _target getVariable ["_unitPosWorld", [0,0,0]];
    private _unit = "C_Soldier_VR_F" createVehicle [-100,-100,0];
    _unit setPosWorld _pos;
    _target setVariable ["_unit", _unit];
    _target setVariable ["_disabled", false, true];

    _unit disableAI "MOVE";
    _unit disableAI "AUTOTARGET";
    _unit disableAI "TARGET";
    _unit disableAI "FSM";
    _unit disableAI "WEAPONAIM";
    _unit disableAI "AUTOCOMBAT";

    _unit setDir ((getDir _target) + 180);
};

medicalAddMinorDamageToUnit = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _unit = _target getVariable ["_unit", objNull];
    [_unit, 0.5, selectRandom ["Head", "Body", "LeftArm", "RightArm", "LeftLeg", "rightleg"], "bullet", _caller] call ace_medical_fnc_addDamageToUnit;
};

medicalAddMediumDamageToUnit = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _unit = _target getVariable ["_unit", objNull];
    [_unit, 0.5, selectRandom ["Head", "Body", "LeftArm", "RightArm", "LeftLeg", "rightleg"], "bullet", _caller] call ace_medical_fnc_addDamageToUnit;
    [_unit, 0.8, selectRandom ["Head", "Body", "LeftArm", "RightArm", "LeftLeg", "rightleg"], "bullet", _caller] call ace_medical_fnc_addDamageToUnit;
};

medicalAddMajorDamageToUnit = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _unit = _target getVariable ["_unit", objNull];
     for "_i" from 0 to 3 do {
        [_unit, 0.8, selectRandom ["Head", "Body", "LeftArm", "RightArm", "LeftLeg", "rightleg"], "bullet", _caller] call ace_medical_fnc_addDamageToUnit;
    };
};

medicalAddRandomDamageToUnit = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _unit = _target getVariable ["_unit", objNull];
    for "_i" from 0 to (3 + (random round 3)) do {
        [_unit, (0.5 + random 0.5), selectRandom ["Head", "Body", "LeftArm", "RightArm", "LeftLeg", "rightleg"], "bullet", _caller] call ace_medical_fnc_addDamageToUnit;
    };
};

medicalFncRespawnUnit = {
   _this call medicalStopStation;
   _this call medicalStartStation;
};

medicalChangeMedicalLevel = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    _caller setVariable ["ace_medical_medicclass", _arguments select 0, true];
};

medicalSpawnBackPack = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    removeBackpack _caller;
    _caller addBackpack "";
    _caller addBackpack "B_Kitbag_rgr";
    for "_i" from 1 to 20 do {
        _caller addItemToBackpack "ACE_fieldDressing";
        _caller addItemToBackpack "ACE_elasticBandage";
        _caller addItemToBackpack "ACE_packingBandage";
        _caller addItemToBackpack "ACE_quikclot";
    };
    for "_i" from 1 to 10 do {_caller addItemToBackpack "ACE_morphine";};
    for "_i" from 1 to 8 do {
        _caller addItemToBackpack "ACE_splint";
        _caller addItemToBackpack "ACE_tourniquet";
    };
    for "_i" from 1 to 6 do {_caller addItemToBackpack "ACE_bloodIV_250";};
    for "_i" from 1 to 5 do {_caller addItemToBackpack "ACE_epinephrine";};
    for "_i" from 1 to 4 do {_caller addItemToBackpack "ACE_bloodIV_500";};
    for "_i" from 1 to 2 do {
        _caller addItemToBackpack "ACE_adenosine";
        _caller addItemToBackpack "ACE_bloodIV";
        _caller addItemToBackpack "ACE_surgicalKit";
        _caller addItemToBackpack "ACE_personalAidKit";
        _caller addItemToBackpack "ACE_plasmaIV_500";
        _caller addItemToBackpack "ACE_salineIV_500";
        _caller addItemToBackpack "ACE_salineIV_250";
        _caller addItemToBackpack "ACE_plasmaIV_250";
    };
};

spawnGrenadeTargets = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    private _targObjArray = [];
    {
        private _unit = "C_Soldier_VR_F" createVehicle [-100,-100,0];
        _unit setPosWorld _x;
        _targObjArray pushBack _unit;
        _unit disableAI "MOVE";
    } forEach (_target getVariable ["_targPosArray", []]);
    _target setVariable ["_targObjArray", _targObjArray];
    _target setVariable ["_targetsAlreadySpawned", true, true];
};
despawnGrenadeTargets = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    {
        deleteVehicle _x;
    } forEach (_target getVariable ["_targObjArray", []]);
    _target setVariable ["_targObjArray", []];
    _target setVariable ["_targetsAlreadySpawned", false, true];
};
addGrenadeRangeEH = {
    (_this select 0) addEventHandler ["Killed", {
        _this spawn {
            params ["_unit"];
            private _pos = getPosWorld _unit;
            private _targObjArray = grenadeRangeSettingsLaptop getVariable ["_targObjArray", []];
            _targObjArray = _targObjArray - [_unit];
            grenadeRangeSettingsLaptop setVariable ["_targObjArray", _targObjArray];
            sleep 5;
            deleteVehicle _unit;
            private _targObjArray = grenadeRangeSettingsLaptop getVariable ["_targObjArray", []];
            private _newUnit = "C_Soldier_VR_F" createVehicle [-100,-100,0];
            _newUnit setPosWorld _pos;
            _targObjArray pushBack _newUnit;
            _newUnit disableAI "MOVE";
            [_newUnit] spawn addGrenadeRangeEH;
            grenadeRangeSettingsLaptop setVariable ["_targObjArray", _targObjArray];
        };
    }];
};

autoRespawnGrenadeTargets = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    {
       [_x] spawn addGrenadeRangeEH;
    } forEach (_target getVariable ["_targObjArray", []]);
};