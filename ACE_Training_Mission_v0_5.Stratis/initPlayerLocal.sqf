/*
	author: stanhope
*/

player setVariable ["PlayerLoadout", getUnitLoadout player];

RYK_fnc_TFAR_LR = {
    [(call TFAR_fnc_activeLrRadio), 1, "30"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 2, "31"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 3, "32"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 4, "33"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 5, "34"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 6, "35"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 7, "36"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 8, "37"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeLrRadio), 9, "38"] call TFAR_fnc_SetChannelFrequency;
};

RYK_fnc_TFAR_SR = {
    [(call TFAR_fnc_activeSwRadio), 1, "40"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 2, "50"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 3, "51"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 4, "52"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 5, "53"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 6, "54"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 7, "60"] call TFAR_fnc_SetChannelFrequency;
    [(call TFAR_fnc_activeSwRadio), 8, "61"] call TFAR_fnc_SetChannelFrequency;
};

player addEventHandler ["InventoryClosed", {
    params ["_unit", "_container"];
    if (_container in [aceMedicalBox_1, aceMedicalBox_2]) then {
        clearItemCargoGlobal _container;
        clearBackpackCargoGlobal _container;
        clearWeaponCargoGlobal _container;
        clearMagazineCargoGlobal _container;
    };
}];
